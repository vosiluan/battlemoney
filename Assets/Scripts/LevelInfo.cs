﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Global_Enum;

[CreateAssetMenu(fileName = "LevelInfo", menuName = "GameInfo/LevelInfo")]
public class LevelInfo : ScriptableObject
{
    public int levelNumber;
    public int turnNumber;
    public int stackNumber;
    public int cardNumberInStack;
    public bool isStack;
    public int cardBlueNumber;
    public int cardRedNumber;
    public int cardSpecialNumber;
    [ShowIf("IsHaveSpecialCard")] public LevelTypeCardSpecial propertySdpecialCard;
    public UnlockType unlockType;
    [ShowIf("IsUnlockCard")] public List<CardInfo> listCardUnlock;
    bool IsHaveSpecialCard()
    {
        if (cardSpecialNumber == 1)
            return true;
        return false;
    }
    bool IsUnlockCard() {
        if (unlockType == UnlockType.UnlockCards) {
            return true;
        }
        return false;
    }
}
