﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIReward : IPopupController
{
    [SerializeField] private Button btnContinue;
    [SerializeField] private TextMeshProUGUI txtTitle;
    [SerializeField] private TextMeshProUGUI txtDescription;
    [SerializeField] private Image imgReward;

    public void Initializer(string title, string description, Sprite icReward)
    {
        txtTitle.text = title;
        txtDescription.text = description;
        imgReward.sprite = icReward;
    }
}
