﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Global_Enum;

public class PlayerAnimController : MonoBehaviour
{
    public enum StateAnimation
    {
        player_idle_003_done, player_scissor, player_rock, player_paper, player_ready_loop, player_pick_v2
    }
    [SerializeField] Animator myAnimator;
    IEnumerator actionReadyLoop;
    Vector3 oriScaleSaved;
    void Awake()
    {
        oriScaleSaved = transform.localScale;
    }
    [Button]
    void TestRollDice_01()
    {
        if (actionReadyLoop != null)
        {
            StopCoroutine(actionReadyLoop);
        }
        actionReadyLoop = DoActionRollScissor();
        StartCoroutine(actionReadyLoop);
    }
    [Button]
    void TestRollDice_02()
    {
        if (actionReadyLoop != null)
        {
            StopCoroutine(actionReadyLoop);
        }
        actionReadyLoop = DoActionRollPaper();
        StartCoroutine(actionReadyLoop);
    }
    [Button]
    void TestRollDice_03()
    {
        if (actionReadyLoop != null)
        {
            StopCoroutine(actionReadyLoop);
        }
        actionReadyLoop = DoActionRollRock();
        StartCoroutine(actionReadyLoop);
    }
    IEnumerator DoActionPickCard()
    {
        myAnimator.SetTrigger(StateAnimation.player_pick_v2.ToString());
        yield return Yielders.Get(1f);
        myAnimator.SetTrigger(StateAnimation.player_idle_003_done.ToString());
        actionReadyLoop = null;
    }
    IEnumerator DoActionRollRock()
    {
        myAnimator.SetTrigger(StateAnimation.player_ready_loop.ToString());
        yield return Yielders.Get(0.5f);
        myAnimator.SetTrigger(StateAnimation.player_rock.ToString());
        yield return Yielders.Get(0.5f);
        myAnimator.SetTrigger(StateAnimation.player_idle_003_done.ToString());
        actionReadyLoop = null;
    }
    IEnumerator DoActionRollPaper()
    {
        myAnimator.SetTrigger(StateAnimation.player_ready_loop.ToString());
        yield return Yielders.Get(0.5f);
        myAnimator.SetTrigger(StateAnimation.player_paper.ToString());
        yield return Yielders.Get(0.5f);
        myAnimator.SetTrigger(StateAnimation.player_idle_003_done.ToString());

        actionReadyLoop = null;
    }
    IEnumerator DoActionRollScissor()
    {
        myAnimator.SetTrigger(StateAnimation.player_ready_loop.ToString());
        yield return Yielders.Get(0.5f);
        myAnimator.SetTrigger(StateAnimation.player_scissor.ToString());
        yield return Yielders.Get(0.5f);
        myAnimator.SetTrigger(StateAnimation.player_idle_003_done.ToString());
        actionReadyLoop = null;
    }
    IEnumerator DoActionIdle()
    {
        myAnimator.SetTrigger(StateAnimation.player_idle_003_done.ToString());
        actionReadyLoop = null;
        yield return null;
    }
    public IEnumerator ShakeDice()
    {
        while (true)
        {
            LeanTween.scale(gameObject, new Vector3(1.7f, 1.7f, 1.7f), 0.4f);
            yield return Yielders.Get(0.4f);
            for (int i = 0; i < 3; i++)
            {
                LeanTween.rotateZ(gameObject, -10, 0.05f);
                yield return Yielders.Get(0.05f);
                LeanTween.rotateZ(gameObject, 10, 0.05f);
                yield return Yielders.Get(0.05f);
            }
            LeanTween.rotateZ(gameObject, 0, 0.05f);
            yield return Yielders.Get(0.1f);
            LeanTween.scale(gameObject, new Vector3(1.4f, 1.4f, 1.4f), 0.4f);
            yield return Yielders.Get(0.4f);
            LeanTween.scale(gameObject, new Vector3(1.5f, 1.5f, 1.5f), 0.2f);
            yield return Yielders.Get(1.5f);
            yield return null;
        }
    }
    public void ActionStart(IEnumerator _doAction)
    {
        if (actionReadyLoop != null)
        {
            StopCoroutine(actionReadyLoop);
        }
        actionReadyLoop = _doAction;
        StartCoroutine(actionReadyLoop);
    }
    public void SetActionShoot(TypeShoot _typeShoot)
    {
        switch (_typeShoot)
        {
            case TypeShoot.Scissor:
                ActionStart(DoActionRollScissor());
                break;
            case TypeShoot.Stone:
                ActionStart(DoActionRollRock());
                break;
            case TypeShoot.Paper:
                ActionStart(DoActionRollPaper());
                break;
        }
    }
    public void SetActionPickCard() {
        ActionStart(DoActionPickCard());
    }
    public void ActionSetIdle()
    {
        if (actionReadyLoop != null)
        {
            StopCoroutine(actionReadyLoop);
        }
        actionReadyLoop = DoActionIdle();
        StartCoroutine(actionReadyLoop);
    }
}
