﻿using System;
using System.Threading.Tasks;
using Root.Default.Script;
using Root.Default.Script.Manager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIMain : IPopupController
{
    [SerializeField] private TextMeshProUGUI txtLevel;
    [SerializeField] private Button btnPlay;
    [SerializeField] private RectTransform leaderboardContent;
    [SerializeField] Transform gamePlay;

    [Range(0f, 1f)] [SerializeField] private float animationDuration = 0.5f;
    [Range(0f, 0.25f)] [SerializeField] private float timeRollFakeSv = 0.05f;

    private void Awake()
    {
        txtLevel.text = $"Level {UserDataManager.Instance.userDataSave.level + 1}";
        LeaderboardAnimation(100, 1);
    }

    public void LeaderboardAnimation(int oldRank, int newRank)
    {
        var p0 = ((RectTransform)leaderboardContent.GetChild(0)).anchoredPosition.y;
        var p1 = ((RectTransform)leaderboardContent.GetChild(1)).anchoredPosition.y;
        var p3 = ((RectTransform)leaderboardContent.GetChild(3)).anchoredPosition.y;
        var p4 = ((RectTransform)leaderboardContent.GetChild(4)).anchoredPosition.y;

        // get self
        LeanTween.scale(leaderboardContent.GetChild(2).gameObject, Vector3.one * 1.25f, animationDuration)
            .setOnComplete(
                async () =>
                {
                    for (int i = 0; i < 16; i++)
                    {
                        RectTransform t0;
                        RectTransform t1;
                        RectTransform t4;
                        RectTransform t3;

                        if (i % 2 == 0)
                        {
                            t0 = (RectTransform)leaderboardContent.GetChild(0);
                            t1 = (RectTransform)leaderboardContent.GetChild(1);
                            t3 = (RectTransform)leaderboardContent.GetChild(3);
                            t4 = (RectTransform)leaderboardContent.GetChild(4);
                        }
                        else
                        {
                            t0 = (RectTransform)leaderboardContent.GetChild(1);
                            t1 = (RectTransform)leaderboardContent.GetChild(0);
                            t3 = (RectTransform)leaderboardContent.GetChild(4);
                            t4 = (RectTransform)leaderboardContent.GetChild(3);
                        }

                        for (int j = 0; j < 5; j++)
                        {
                            if (j != 2)
                            {
                                leaderboardContent.GetChild(j).GetChild(1).GetComponent<TextMeshProUGUI>()
                                        .text =
                                    App.RandomString();
                            }
                        }

                        LeanTween.moveY((RectTransform)t1.transform, p0, timeRollFakeSv);
                        LeanTween.moveY((RectTransform)t0.transform, p1, timeRollFakeSv);
                        LeanTween.moveY((RectTransform)t4.transform, p3, timeRollFakeSv);
                        LeanTween.moveY((RectTransform)t3.transform, p4, timeRollFakeSv);
                        await Task.WhenAll(Task.Delay(TimeSpan.FromSeconds(animationDuration)));
                    }

                    await Task.WhenAll(Task.Delay(TimeSpan.FromSeconds(animationDuration)));
                    LeanTween.scale(leaderboardContent.GetChild(2).gameObject, Vector3.one, animationDuration);
                });
    }
    public void OnBtnSettingClick() {
        UISetting _popup = PopupManager.Instance.CreatePopupSetting();
        _popup.Show();
    }
    public void OnBtnPlayClicked() {
        gamePlay.gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}