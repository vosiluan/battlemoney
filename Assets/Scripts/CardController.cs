﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static Global_Enum;
public class CardController : MonoBehaviour
{
    public CardInfo cardInfo;
    [SerializeField] TextMeshProUGUI txtValue;
    public AutoFollow follow;
    public void Init(bool _isCanChoose) {
        string _txtValueResult = "";
        switch (cardInfo.typeCard) {
            case TypeCard.BlueCard:
                _txtValueResult = (cardInfo.propertyCard == PropertyCard.bluePlus) ? "+" + cardInfo.value + "$" :
                                  (cardInfo.propertyCard == PropertyCard.bluePlusPercent) ? "+" + cardInfo.value + "%" :
                                  (cardInfo.propertyCard == PropertyCard.blueMultiply) ? "X" + cardInfo.value : "";
                break;
            case TypeCard.RedCard:
                _txtValueResult = (cardInfo.propertyCard == PropertyCard.redMinusPercent) ? "-" + cardInfo.value + "%" :
                                  (cardInfo.propertyCard == PropertyCard.redSteal) ? "steal " + cardInfo.value + "%" :
                                  (cardInfo.propertyCard == PropertyCard.redDivide) ? "÷" + cardInfo.value : "";
                break;
            case TypeCard.SpecialCard:
                _txtValueResult = (cardInfo.propertyCard == PropertyCard.specialMoves) ? "+" + cardInfo.value + " moves" :
                                  (cardInfo.propertyCard == PropertyCard.specialRandom) ? "?" : "";
                break;
        }
        txtValue.text = _txtValueResult;
        cardInfo.isCanChoose = _isCanChoose;
    }

    private void OnMouseDown()
    {
        StartCoroutine(OnBtnCardClick());
        //OnBtnCardClick();
    }

    public IEnumerator OnBtnCardClick(){
        if (IngameManager.instance.playerMe.isMyTurn) {
            if (cardInfo.isCanChoose) {
                if (cardInfo.typeCard == TypeCard.RedCard)
                {
                    IngameManager.instance.playerMe.playerAnim.SetActionPickCard();
                    yield return Yielders.Get(1f);
                    StartCoroutine(AnimationCardMove(IngameManager.instance.playerOpponent.player, () =>
                    {
                        IngameManager.instance.HandleChooseCard(cardInfo);
                        IngameManager.instance.listCardsIngame.Remove(this);
                        IngameManager.instance.ChangeKey();
                        IngameManager.instance.RemoveCardInStack(this);
                        Destroy(gameObject);
                    }));
                }
                else
                {
                    IngameManager.instance.playerMe.playerAnim.SetActionPickCard();
                    yield return Yielders.Get(1f);
                    StartCoroutine(AnimationCardMove(IngameManager.instance.playerMe, () =>
                    {
                        IngameManager.instance.HandleChooseCard(cardInfo);
                        IngameManager.instance.listCardsIngame.Remove(this);
                        IngameManager.instance.ChangeKey();
                        IngameManager.instance.RemoveCardInStack(this);
                        Destroy(gameObject);
                    }));
                }
            }
        }
    }
    public IEnumerator AnimationCardMove(IngameManager.PlayerGroup _player,System.Action _callBack) {
        follow.target = null;
        LeanTween.move(gameObject, _player.posCardMove.transform.position, 0.3f);
        yield return Yielders.Get(1f);
        _callBack();
        yield return null;
    }
}
