using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Root.Default.Script
{
    [Serializable]
    public class UniDictionary<TKey, TValue> : IDictionary<TKey, TValue>, ISerializationCallbackReceiver
    {
        // Serializable KeyValuePair struct
        [Serializable]
        private struct KeyValuePair
        {
            public TKey key;
            public TValue value;

            public KeyValuePair(TKey key, TValue value)
            {
                this.key = key;
                this.value = value;
            }
        }

        // Internal
        [SerializeField] private List<KeyValuePair> list = new List<KeyValuePair>();

        public ICollection<TKey> Keys => _dt.Keys;
        public ICollection<TValue> Values => _dt.Values;

        private Dictionary<TKey, TValue> _dt = new Dictionary<TKey, TValue>();
        private Dictionary<TKey, int> _indexByKey = new Dictionary<TKey, int>();

        // IDictionary
        public TValue this[TKey key]
        {
            get => _dt[key];
            set
            {
                _dt[key] = value;

                if (_indexByKey.ContainsKey(key))
                {
                    list[_indexByKey[key]] = new KeyValuePair(key, value);
                }
                else
                {
                    list.Add(new KeyValuePair(key, value));
                    _indexByKey.Add(key, list.Count - 1);
                }
            }
        }

        public bool IsReadOnly { get; set; }

        // ICollection
        public int Count => _dt.Count;

#region MonoBehaviour

#endregion

#region Public

        public void Add(TKey key, TValue value)
        {
            _dt.Add(key, value);
            list.Add(new KeyValuePair(key, value));
            _indexByKey.Add(key, list.Count - 1);
        }

        public bool ContainsKey(TKey key)
        {
            return _dt.ContainsKey(key);
        }

        public bool Remove(TKey key)
        {
            if (!_dt.Remove(key))
            {
                return false;
            }

            var index = _indexByKey[key];

            list.RemoveAt(index);
            UpdateIndexes(index);
            _indexByKey.Remove(key);
            return true;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return _dt.TryGetValue(key, out value);
        }

        public void Add(KeyValuePair<TKey, TValue> pair)
        {
            Add(pair.Key, pair.Value);
        }

        public void Clear()
        {
            _dt.Clear();
            list.Clear();
            _indexByKey.Clear();
        }

        public bool Contains(KeyValuePair<TKey, TValue> pair)
        {
            return _dt.TryGetValue(pair.Key, out var value) &&
                   EqualityComparer<TValue>.Default.Equals(value, pair.Value);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            if (array == null)
            {
                throw new ArgumentException("The array can't be null.");
            }

            if (arrayIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(array));
            }

            if (array.Length - arrayIndex < _dt.Count)
            {
                throw new ArgumentException("The destination array has fewer elements than the collection.");
            }

            foreach (var pair in _dt)
            {
                array[arrayIndex] = pair;
                arrayIndex++;
            }
        }

        public bool Remove(KeyValuePair<TKey, TValue> pair)
        {
            try
            {
                var value = _dt[pair.Key];
                return value != null && EqualityComparer<TValue>.Default.Equals(value, pair.Value) && Remove(pair.Key);
            }
            catch (Exception)
            {
                //Log.Error($"{pair.Key} doesn't exists in dictionary");
                return false;
            }
        }

        // IEnumerable
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return _dt.GetEnumerator();
        }

        // Since lists can be serialized natively by unity no custom implementation is needed
        public void OnBeforeSerialize()
        {
        }

        // Fill dictionary with list pairs and flag key-collisions.
        public void OnAfterDeserialize()
        {
            _dt.Clear();
            _indexByKey.Clear();

            for (var i = 0; i < list.Count; i++)
            {
                var key = list[i].key;

                if (key == null ||
                    ContainsKey(key))
                {
                    continue;
                }

                _dt.Add(key, list[i].value);
                _indexByKey.Add(key, i);
            }
        }

#endregion

#region Private

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _dt.GetEnumerator();
        }

        private void UpdateIndexes(int removedIndex)
        {
            for (var i = removedIndex; i < list.Count; i++)
            {
                _indexByKey[list[i].key]--;
            }
        }

#endregion

#region Protected

#endregion
    }
}