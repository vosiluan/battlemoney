﻿using System;

namespace Root.Default.Script.Model
{
    [Serializable]
    public class UserDataSave
    {
        public int level;

        public float gold;
        public float diamond;

        public float soundVolume;
        public float musicVolume;

        public bool vibration;
        public bool removedAds;
    }
}