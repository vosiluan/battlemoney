﻿#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
using System.Security;
#endif
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Root.Default.Script.Base;
using UnityEngine;
using UnityEngine.UI;

namespace Root.Default.Script.Manager
{
    [DefaultExecutionOrder(3)]
    public class AudioManager : BaseSingleton<AudioManager>
    {
        public UniDictionary<string, AudioClip> dictionaries = new UniDictionary<string, AudioClip>();

        [SerializeField] private List<AudioClip> audioClips = new List<AudioClip>();

        [Header("[AUDIO SOURCE]")]
        [SerializeField]
        private AudioSource audioSound;

        [SerializeField] private AudioSource audioMusic;

        [Header("[SOUND]")][SerializeField] private Sprite icSoundOn;

        [SerializeField] private Sprite icSoundOff;

        [Range(0f, 1f)][SerializeField] private float minSoundVolume;
        [Range(0f, 1f)][SerializeField] private float maxSoundVolume = 1f;

        [Header("[MUSIC]")][SerializeField] private Sprite icMusicOn;

        [SerializeField] private Sprite icMusicOff;

        [Range(0f, 1f)][SerializeField] private float minMusicVolume;
        [Range(0f, 1f)][SerializeField] private float maxMusicVolume = 1f;

        [Header("[VIBRATION]")]
        [SerializeField]
        private Sprite icVibrationOn;

        [SerializeField]
        private Sprite icVibrationOff;

        private Button _btnMusic;
        private Button _btnSound;
        private Button _btnVibration;

        private Slider _sliderMusic;
        private Slider _sliderSound;

        private Image _imgMusic;
        private Image _imgSound;
        private Image _imgVibration;

        private int _loop;

#region MonoBehaviour

#if UNITY_EDITOR
        private void OnValidate()
        {
            dictionaries.Clear();

            foreach (var x in audioClips)
            {
                dictionaries.Add(x.name, x);
            }

            audioMusic.volume = maxMusicVolume;
            audioSound.volume = maxSoundVolume;
        }
#endif

#endregion

#region Public

        public void SetupNewSound(Button bSound, Image iSound, Slider sSound = null)
        {
            _sliderSound = sSound;
            _btnSound = bSound;
            _imgSound = iSound;
        }

        public void SetupNewMusic(Button bMusic, Image iMusic, Slider sMusic = null)
        {
            _sliderMusic = sMusic;
            _btnMusic = bMusic;
            _imgMusic = iMusic;
        }

        public void SetupNewVibration(Button bVibration, Image iVibration)
        {
            _btnVibration = bVibration;
            _imgVibration = iVibration;
        }

        public void PlaySound(string clipName, int loopTimes = 1)
        {
            if (audioSound.clip != null &&
                clipName.Equals(audioSound.clip.name))
            {
                return;
            }

            var clip = GetClip(clipName);

            if (clip == null)
            {
                return;
            }

            if (loopTimes < 2)
            {
                audioSound.PlayOneShot(clip);
            }
            else
            {
                _loop = 0;
                audioSound.clip = clip;

                Loop(clip, loopTimes);
            }
        }

        public void PlayMusic(string clipName)
        {
            if (audioMusic.clip != null &&
                clipName.Equals(audioMusic.clip.name))
            {
                return;
            }

            var clip = GetClip(clipName);

            if (clip == null)
            {
                return;
            }

            audioMusic.clip = clip;

            audioMusic.Play();
        }

        public void StopMusic()
        {
            audioMusic.Stop();
        }

        public void StopSound()
        {
            audioSound.Stop();
        }

        public static void PlayVibrate()
        {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            if (!UserDataManager.Instance.userDataSave.vibration)
            {
                return;
            }

            try
            {
                AndroidVibrate.Vibrate();
            }
            catch (SecurityException)
            {
                Debug.Log("You need add vibrate permission to AndroidManifest.xml in your project, " +
                          "add this line: <uses-permission android:name=\"android.permission.VIBRATE\"/>");
            }

#endif
        }

        public void ChangeSoundVolume(float volume)
        {
            audioSound.volume = volume;

            if (audioMusic != null)
            {
                audioMusic.volume = volume;
            }

            UserDataManager.Instance.SaveSoundVolume(volume);
            ChangeImgSound();

            if (volume <= 0f)
            {
                audioSound.Stop();

                if (audioMusic != null)
                {
                    audioMusic.Stop();
                }
            }
            else
            {
                if (audioMusic != null &&
                    !audioMusic.isPlaying)
                {
                    audioMusic.Play();
                }
            }
        }

        public void ChangeMusicVolume(float volume)
        {
            audioMusic.volume = volume;

            UserDataManager.Instance.SaveMusicVolume(volume);
            ChangeImgMusic();

            if (volume <= 0f)
            {
                audioMusic.Stop();
            }
            else
            {
                if (!audioMusic.isPlaying)
                {
                    audioMusic.Play();
                }
            }
        }

        public void InitializerAudio()
        {
            UserDataManager.Instance.SaveSoundVolume(maxSoundVolume);
            UserDataManager.Instance.SaveMusicVolume(audioMusic != null ? maxMusicVolume : maxSoundVolume);

            if (_btnSound != null)
            {
                ChangeImgSound();

                if (_sliderSound != null)
                {
                    _sliderSound.value = UserDataManager.Instance.userDataSave.soundVolume;
                }

                _btnSound.onClick.RemoveAllListeners();

                _btnSound.onClick.AddListener(
                    () =>
                    {
                        UserDataManager.Instance.SaveSoundVolume(
                            audioSound.volume > 0f ? minSoundVolume : maxSoundVolume);

                        PlaySound("click");
                        ChangeImgSound();

                        if (_sliderSound == null)
                        {
                            return;
                        }

                        if (_sliderMusic == null)
                        {
                            _sliderSound.value = UserDataManager.Instance.userDataSave.soundVolume;
                            return;
                        }

                        if (_sliderMusic.name != _sliderSound.name)
                        {
                            _sliderSound.value = UserDataManager.Instance.userDataSave.soundVolume;
                            return;
                        }

                        _sliderSound.SetValueWithoutNotify(UserDataManager.Instance.userDataSave.soundVolume);

                        if (audioMusic != null)
                        {
                            _sliderMusic.SetValueWithoutNotify(UserDataManager.Instance.userDataSave.soundVolume);
                        }
                    });
            }

            if (_btnMusic != null)
            {
                ChangeImgMusic();

                if (_sliderMusic != null)
                {
                    _sliderMusic.value = UserDataManager.Instance.userDataSave.musicVolume;
                }

                _btnMusic.onClick.RemoveAllListeners();

                _btnMusic.onClick.AddListener(
                    () =>
                    {
                        UserDataManager.Instance.SaveMusicVolume(
                            audioMusic.volume > 0f ? minMusicVolume : maxMusicVolume);

                        PlaySound("click");
                        ChangeImgMusic();

                        if (_sliderMusic == null)
                        {
                            return;
                        }

                        if (_sliderSound == null)
                        {
                            _sliderMusic.value = UserDataManager.Instance.userDataSave.musicVolume;
                            return;
                        }

                        if (_sliderMusic.name != _sliderSound.name)
                        {
                            _sliderMusic.value = UserDataManager.Instance.userDataSave.musicVolume;
                        }
                        else
                        {
                            _sliderMusic.SetValueWithoutNotify(UserDataManager.Instance.userDataSave.musicVolume);
                            _sliderSound.SetValueWithoutNotify(UserDataManager.Instance.userDataSave.musicVolume);
                        }
                    });
            }

            if (_btnVibration != null)
            {
                ChangeImgVibration();
                _btnVibration.onClick.RemoveAllListeners();

                _btnVibration.onClick.AddListener(
                    () =>
                    {
                        UserDataManager.Instance.SetVibration();
                        PlaySound("click");
                        ChangeImgVibration();
                    });
            }

            if (_sliderMusic != null)
            {
                _sliderMusic.value = UserDataManager.Instance.userDataSave.musicVolume;

                if (_sliderMusic.name != _sliderSound.name)
                {
                    _sliderMusic.onValueChanged.RemoveAllListeners();
                }

                _sliderMusic.onValueChanged.AddListener(ChangeMusicVolume);
            }

            if (_sliderSound == null)
            {
                return;
            }

            _sliderSound.value = UserDataManager.Instance.userDataSave.soundVolume;

            if (_sliderMusic.name != _sliderSound.name)
            {
                _sliderSound.onValueChanged.RemoveAllListeners();
            }

            _sliderSound.onValueChanged.AddListener(ChangeSoundVolume);
        }

#endregion

#region Private

        private void OnChangeSound()
        {
            audioSound.volume = UserDataManager.Instance.userDataSave.soundVolume;

            if (audioSound.volume <= 0f)
            {
                audioSound.Stop();

                if (audioMusic != null)
                {
                    audioMusic.Stop();
                    return;
                }
            }

            if (audioMusic != null)
            {
                OnChangeMusic();
            }
        }

        private void OnChangeMusic()
        {
            audioMusic.volume = audioMusic == null
                                    ? UserDataManager.Instance.userDataSave.soundVolume
                                    : UserDataManager.Instance.userDataSave.musicVolume;

            if (audioMusic.volume >= 1f)
            {
                audioMusic.Play();
            }
            else
            {
                audioMusic.Stop();
            }
        }

        private void ChangeImgSound()
        {
            _imgSound.sprite = UserDataManager.Instance.HasSound ? icSoundOn : icSoundOff;

            OnChangeSound();
        }

        private void ChangeImgMusic()
        {
            _imgMusic.sprite = UserDataManager.Instance.HasMusic ? icMusicOn : icMusicOff;

            OnChangeMusic();
        }

        private void ChangeImgVibration()
        {
            _imgVibration.sprite = UserDataManager.Instance.userDataSave.vibration
                                       ? icVibrationOn
                                       : icVibrationOff;
        }

        private AudioClip GetClip(string soundName)
        {
#if UNITY_EDITOR
            try
            {
#endif
                return dictionaries[soundName];
#if UNITY_EDITOR
            }
            catch (Exception)
            {
                //Log.Error($"{soundName} doesn't exists in list audio");
                return null;
            }
#endif
        }

        private void Loop(AudioClip clip, int loop)
        {
            while (true)
            {
                _loop += 1;

                Task.Delay(TimeSpan.FromSeconds(clip.length / 2));

                if (_loop < loop)
                {
                    continue;
                }

                break;
            }
        }

#endregion

#region Protected

#endregion
    }
}