﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Global_Enum;

[System.Serializable]
public class CardInfo
{
    public TypeCard typeCard;
    public PropertyCard propertyCard;
    public int value;
    public bool isCanChoose;
}
