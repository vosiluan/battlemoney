﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StackController : MonoBehaviour
{
    public List<CardController> listCard;

    public void RefreshCardCanChoose() {
        for (int i =0; i< listCard.Count; i++) {
            if (i == listCard.Count - 1)
            {
                listCard[i].cardInfo.isCanChoose = true;
            }
            else {
                listCard[i].cardInfo.isCanChoose = false;
            }
        }
    }
}
