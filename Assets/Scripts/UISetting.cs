﻿using System;
using System.Collections;
using System.Collections.Generic;
using Root.Default.Script.Manager;
using UnityEngine;
using UnityEngine.UI;

public class UISetting : IPopupController
{
    [SerializeField] private Button btnSound;
    [SerializeField] private Button btnHaptic;
    [SerializeField] private Button btnRestore;
    [SerializeField] private Button btnGdpr;
    [SerializeField] private Button btnClose;

    [SerializeField] private Image imgSound;
    [SerializeField] private Image imgHaptic;

    private void Start()
    {
        AudioManager.Instance.SetupNewSound(btnSound, imgSound);
        AudioManager.Instance.SetupNewVibration(btnHaptic, imgHaptic);
        AudioManager.Instance.InitializerAudio();
    }
}
