﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIWin : IPopupController
{
    public Button btnComplete;

    private void Awake()
    {
        //btnComplete.onClick.AddListener(() => SceneManager.LoadScene(SceneManager.GetActiveScene().name));
    }
    public void OnBtnTabCLick() {
        if (IngameManager.instance.currentLevelNumber ==1) {
            IngameManager.instance.currentLevelNumber = 0;
        }
        else
            IngameManager.instance.currentLevelNumber++;
        IngameManager.instance.ClearTable();
        IngameManager.instance.InitForNewGame();
        gameObject.SetActive(false);
    }
}