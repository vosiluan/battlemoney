﻿using UnityEngine;

namespace Root.Default.Script.Base
{
    public class BaseSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        [SerializeField] private bool isDontDestroy;

        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<T>();
                }

                return _instance;
            }

            private set => _instance = value;
        }

        protected virtual void Awake()
        {
            if (Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            if (!isDontDestroy)
            {
                return;
            }

            if (transform.parent != null)
            {
                transform.SetParent(null);
            }

            DontDestroyOnLoad(gameObject);
        }

        protected virtual void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
            }

            DoKill();
        }

        protected virtual void OnDisable()
        {
            DoKill();
        }

        protected virtual void DoKill()
        {
        }

#if UNITY_EDITOR
        public virtual void Setup()
        {
        }
#endif
    }
}