﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static Global_Enum;

[System.Serializable]
public class IngameManager : MonoBehaviour
{
    [SerializeField] List<LineGroupController> listLineGroup;

    public static IngameManager instance;
    [SerializeField] Button btnScissor;
    [SerializeField] Button btnStone;
    [SerializeField] Button btnPaper;
    [SerializeField] Transform btnGroup;
    public List<Sprite> listSpriteResult;

    [SerializeField]
    List<CardInfo> listCardsPlayer; // Cần save thông tin này khi off game. Load lên khi mở game lần tiếp theo.

    [SerializeField] Text txtLevel;
    [SerializeField] List<LevelInfo> listLevels;
    [SerializeField] GameObject placeHolderCardPrefab;
    [SerializeField] Transform cardGroup;
    [SerializeField] Transform cardTable;

    [Header("Table")]
    /*[SerializeField] StackController stackPrefab;*/
    [SerializeField]
    CardController blueCardPrefab;
    [SerializeField] CardController redCardPrefab;
    [SerializeField] CardController specialCardPrefab;
    public int currentLevelNumber;
    [Header("UI")]
    [SerializeField] UIWin uiWin;
    [SerializeField] UILose uiLose;

    [Header("Table")][SerializeField] List<CardInfo> listBlueCards;
    [SerializeField] List<CardInfo> listRedCards;
    [SerializeField] List<CardInfo> listSpecialCards;
    List<GameObject> listLine;
    public List<CardController> listCardsIngame;
    List<StackController> listStack;
    List<GameObject> listCardHolder;
    GameStatus gameStatus;
    IEnumerator actionCheckEndgame;
    LineGroupController lineGroupCurrent;
    [System.Serializable]
    public class PlayerGroup
    {
        //public Image imgResult;
        public TextMeshProUGUI txtMoney;
        public long money;
        public bool isMyTurn;
        public TypeShoot typeShoot;
        public Transform posCardMove;
        public PlayerAnimController playerAnim;
        public Transform panelFlag;
        public void InitStartGame()
        {
            money = 10;
            isMyTurn = false;
            panelFlag.gameObject.SetActive(false);
        }

        //public void SetResultShootImg(Sprite _sprite)
        //{
        //    imgResult.sprite = _sprite;
        //    imgResult.gameObject.SetActive(true);
        //}

        //public IEnumerator AnimShowResultShoot()
        //{
        //    imgResult.transform.localScale = Vector3.zero;
        //    LeanTween.scale(imgResult.gameObject, Vector3.one * 1.2f, 0.2f);
        //    yield return Yielders.Get(0.2f);
        //    LeanTween.scale(imgResult.gameObject, Vector3.one, 0.2f);
        //    yield return Yielders.Get(1f);
        //    imgResult.gameObject.SetActive(false);
        //}
    }

    public PlayerGroup playerMe;
    public AIPlayer playerOpponent;

    [Button]
    void Test()
    {
        
    }

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        Init();
    }

    void Init()
    {
        InitListCardsFirst();
        InitForNewGame();
    }

    public void InitForNewGame()
    {
        gameStatus = GameStatus.Wait;
        playerMe.InitStartGame();
        playerOpponent.player.InitStartGame();
        txtLevel.text = "Level " + listLevels[currentLevelNumber].levelNumber;
        txtLevel.gameObject.SetActive(true);
        playerOpponent.InitAI();
        listLine = new List<GameObject>();
        listStack = new List<StackController>();
        listCardsIngame = new List<CardController>();
        listCardHolder = new List<GameObject>();

        SetupCardForNewGame();
    }

    void InitListCardsFirst()
    {
        listCardsPlayer = new List<CardInfo>();

        List<int> _valueCardsPlus = new List<int>()
                                    {
                                        10,
                                        20,
                                        25,
                                        40,
                                        50,
                                        75,
                                        100
                                    };

        List<int> _valueCardsPlusPercen = new List<int>()
                                          {
                                              25,
                                              50,
                                              100,
                                              200,
                                              400,
                                              500,
                                              1000
                                          };

        List<int> _valueCardsMultiply = new List<int>()
                                        {
                                            2,
                                            4,
                                            5,
                                            8,
                                            10
                                        };

        GenerateCard(_valueCardsPlus, PropertyCard.bluePlus, TypeCard.BlueCard);
        GenerateCard(_valueCardsPlusPercen, PropertyCard.bluePlusPercent, TypeCard.BlueCard);
        GenerateCard(_valueCardsMultiply, PropertyCard.blueMultiply, TypeCard.BlueCard);
    }

    void SetupCardForNewGame()
    {
        listBlueCards = new List<CardInfo>();
        listRedCards = new List<CardInfo>();
        listSpecialCards = new List<CardInfo>();
        listBlueCards = GetCardInfoByTypeCard(TypeCard.BlueCard);
        listRedCards = GetCardInfoByTypeCard(TypeCard.RedCard);
        listSpecialCards = GetCardInfoByTypeCard(TypeCard.SpecialCard);
    }

    void GenerateCard(List<int> _listValueCards, PropertyCard _propertyCard, TypeCard _typeCard)
    {
        CardInfo _card = new CardInfo();

        for (int i = 0; i < _listValueCards.Count; i++)
        {
            _card = new CardInfo();
            _card.typeCard = _typeCard;
            _card.propertyCard = _propertyCard;
            _card.value = _listValueCards[i];
            listCardsPlayer.Add(_card);
        }
    }

    List<CardInfo> GetCardInfoByTypeCard(TypeCard _typeCard)
    {
        List<CardInfo> _listCardResult = new List<CardInfo>();

        for (int i = 0; i < listCardsPlayer.Count; i++)
        {
            if (listCardsPlayer[i].typeCard == _typeCard)
            {
                _listCardResult.Add(listCardsPlayer[i]);
            }
        }

        return _listCardResult;
    }

    public IEnumerator CompareShoot()
    {
        if (playerOpponent.resultShoot == ResultShoot.lose ||
            playerOpponent.resultShoot == ResultShoot.drawToLose)
        {
            playerMe.isMyTurn = true;
            playerMe.panelFlag.gameObject.SetActive(true);
            playerOpponent.player.panelFlag.gameObject.SetActive(false);
        }
        else if (playerOpponent.resultShoot == ResultShoot.win ||
                 playerOpponent.resultShoot == ResultShoot.drawToWin)
        {
            playerOpponent.player.isMyTurn = true;
            playerMe.panelFlag.gameObject.SetActive(false);
            playerOpponent.player.panelFlag.gameObject.SetActive(true);
        }
        gameStatus = GameStatus.Start;
        Debug.LogError("StartGame");
        yield return Yielders.Get(1f);
        DrawnCardsLevel(listLevels[currentLevelNumber]);
        btnGroup.gameObject.SetActive(false);
        playerMe.txtMoney.gameObject.SetActive(true);
        playerMe.txtMoney.text = playerMe.money.ToString() + " $";
        playerOpponent.player.txtMoney.gameObject.SetActive(true);
        playerOpponent.player.txtMoney.text = playerOpponent.player.money.ToString() + " $";
        playerOpponent.StartAI();
        StartActionCheckEndGame();
    }

    void DrawnCardsLevel(LevelInfo _level)
    {
        int _lineNumber = (_level.stackNumber > 4) ? 2 : 1;
        GameObject _line;

        //Add line to table

        //Add Stack to line.
        if (_level.stackNumber == 5)
        {
            //AddStackToTable(3, listLine[0].transform);
            //AddStackToTable(2, listLine[1].transform);
            lineGroupCurrent = Instantiate(listLineGroup[2], cardTable);
        }
        else if (_level.stackNumber == 8)
        {
            lineGroupCurrent = Instantiate(listLineGroup[0], cardTable);
        }
        else
        {
            lineGroupCurrent = Instantiate(listLineGroup[1], cardTable);
        }
        listStack.AddRange(lineGroupCurrent.stacks);
        // Add Card to stack.
        List<CardInfo> _listBlueCardDraw = (listBlueCards.Count > 0)
                                               ? RandomCard(_level.cardBlueNumber, listBlueCards)
                                               : new List<CardInfo>();

        List<CardInfo> _listRedCardDraw = (listRedCards.Count > 0)
                                              ? RandomCard(_level.cardRedNumber, listRedCards)
                                              : new List<CardInfo>();

        List<CardInfo> _listSpecialCardDraw = (listSpecialCards.Count > 0)
                                                  ? RandomCard(_level.cardSpecialNumber, listSpecialCards)
                                                  : new List<CardInfo>();

        List<CardInfo> _totalCard = new List<CardInfo>();
        _totalCard.AddRange(_listBlueCardDraw);
        _totalCard.AddRange(_listRedCardDraw);
        _totalCard.AddRange(_listSpecialCardDraw);
        _totalCard = MergeList(_totalCard);
        AddCardToStack(_totalCard, _level.cardNumberInStack);
    }

    void AddStackToTable(int _stackNumber, Transform _lineTransform)
    {
        /*StackController _stack;

        for (int i = 0; i < _stackNumber; i++)
        {
            _stack = Instantiate(stackPrefab, _lineTransform);
            listStack.Add(_stack);
        }*/
    }

    void AddCardToStack(List<CardInfo> _listTotalCards, int _cardNumberInStack)
    {
        CardController _card = new CardController();
        GameObject _holder = new GameObject();
        int _tempCount = _cardNumberInStack;
        int _indexStack = 0;

        for (int i = 0; i < _listTotalCards.Count; i++)
        {
            if (_tempCount == 0)
            {
                if (_indexStack < listStack.Count - 1)
                {
                    _indexStack++;
                    _tempCount = _cardNumberInStack;
                }
            }

            _card = Instantiate(
                (_listTotalCards[i].typeCard == TypeCard.BlueCard) ? blueCardPrefab :
                (_listTotalCards[i].typeCard == TypeCard.RedCard) ? redCardPrefab : specialCardPrefab, cardGroup);

            _holder = Instantiate(placeHolderCardPrefab, listStack[_indexStack].transform);
            listCardHolder.Add(_holder);
            _card.cardInfo = _listTotalCards[i];
            _card.follow.target = _holder.transform;
            _card.Init(true);
            listCardsIngame.Add(_card);
            listStack[_indexStack].listCard.Add(_card);
            _tempCount--;
        }

        if (listLevels[currentLevelNumber].isStack)
        {
            for (int i = 0; i < listStack.Count; i++)
            {
                listStack[i].RefreshCardCanChoose();
            }
        }
    }

    void StartActionCheckEndGame()
    {
        StopActionCheckEndgame();
        actionCheckEndgame = CheckEndGame();
        StartCoroutine(actionCheckEndgame);
    }

    void StopActionCheckEndgame()
    {
        if (actionCheckEndgame != null)
        {
            StopCoroutine(actionCheckEndgame);
            actionCheckEndgame = null;
        }
    }

    public void ClearTable()
    {
        DestroyListGameObj(listLine);
        DestroyListGameObj(listCardHolder);
        //for (int i =0; i< listStack.Count;i++) {
        //    Destroy(listStack[i].gameObject);
        //}
        playerMe.txtMoney.gameObject.SetActive(false);
        playerMe.panelFlag.gameObject.SetActive(false);
        playerOpponent.player.txtMoney.gameObject.SetActive(false);
        playerOpponent.player.panelFlag.gameObject.SetActive(false);
        btnGroup.gameObject.SetActive(true);
        StopActionCheckEndgame();
    }

    void DestroyListGameObj(List<GameObject> _listObj)
    {
        for (int i = 0; i < _listObj.Count; i++)
        {
            Destroy(_listObj[i]);
        }
    }

    public void RemoveCardInStack(CardController _card)
    {
        for (int i = 0; i < listStack.Count; i++)
        {
            for (int j = 0; j < listStack[i].listCard.Count; j++)
            {
                if (listStack[i].listCard[j] == _card)
                {
                    listStack[i].listCard.RemoveAt(j);
                    listStack[i].RefreshCardCanChoose();
                    break;
                }
            }
        }
    }

    IEnumerator CheckEndGame()
    {
        while (gameStatus == GameStatus.Start)
        {
            if (listCardsIngame.Count == 0)
            {
                gameStatus = GameStatus.Finish;
                playerOpponent.StopAI();
                yield return Yielders.Get(2f);
                if (playerMe.money > playerOpponent.player.money){
                    Debug.LogError("PlayerMe Win");
                    uiWin.gameObject.SetActive(true);
                    playerMe.panelFlag.gameObject.SetActive(false);
                    playerOpponent.player.panelFlag.gameObject.SetActive(false);
                }
                else
                {
                    Debug.LogError("PlayerOppent Win");
                    uiLose.gameObject.SetActive(true);
                    playerMe.panelFlag.gameObject.SetActive(false);
                    playerOpponent.player.panelFlag.gameObject.SetActive(false);
                }

                switch (listLevels[currentLevelNumber].unlockType)
                {
                    case UnlockType.UnlockCards:
                        /*Show popup UnlockCards*/
                        Debug.Log("UnlockCard");
                        /*Add cards were unlocked to list cards player and save*/
                        listCardsPlayer.AddRange(listLevels[currentLevelNumber].listCardUnlock);
                        break;

                    case UnlockType.UnlockStack:
                        /*Show popup UnclockStack*/
                        Debug.Log("UnlockStack");
                        break;
                }
                Destroy(lineGroupCurrent.gameObject);
            }
            yield return null;
        }
    }

    List<CardInfo> RandomCard(int _cardNumber, List<CardInfo> _listCard)
    {
        int _randomNumber;
        List<CardInfo> _listCardDraw = new List<CardInfo>();

        for (int i = 0; i < _cardNumber; i++)
        {
            _randomNumber = Random.Range(0, _listCard.Count);
            _listCardDraw.Add(_listCard[_randomNumber]);
            _listCard.Remove(_listCard[_randomNumber]);
        }

        return _listCardDraw;
    }

    List<CardInfo> MergeList(List<CardInfo> _listCard)
    {
        int _index = 0;

        for (int i = 0; i < _listCard.Count; i++)
        {
            _index = Random.Range(0, _listCard.Count);
            Swap(_listCard, i, _index);
        }

        return _listCard;
    }

    public void Swap(List<CardInfo> list, int indexA, int indexB)
    {
        if (indexB > -1 &&
            indexB < list.Count)
        {
            CardInfo tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
        }
    }

    public void HandleChooseCard(CardInfo _cardInfo)
    {
        long _money = 0;
        PlayerGroup _curentPlayer = GetCurrentPlayer(true);

        switch (_cardInfo.typeCard)
        {
            case TypeCard.BlueCard:
                _money = (_cardInfo.propertyCard == PropertyCard.bluePlus)
                             ? _cardInfo.value
                             : (_cardInfo.propertyCard == PropertyCard.bluePlusPercent)
                                 ? (_curentPlayer.money * _cardInfo.value) / 100
                                 : (_cardInfo.propertyCard == PropertyCard.blueMultiply)
                                     ? (_curentPlayer.money * (_cardInfo.value - 1))
                                     : 0;

                _curentPlayer.money += _money;
                break;

            case TypeCard.RedCard:
                PlayerGroup _unCurentPlayer = GetCurrentPlayer(false);
                long _moneyPlus = 0;

                if (_cardInfo.propertyCard == PropertyCard.redDivide)
                {
                    _unCurentPlayer.money = _unCurentPlayer.money / _cardInfo.value;
                }
                else if (_cardInfo.propertyCard == PropertyCard.redMinusPercent)
                {
                    _money = (_unCurentPlayer.money * _cardInfo.value) / 100;
                }
                else
                {
                    _money = (_unCurentPlayer.money * _cardInfo.value) / 100;
                    _moneyPlus = _money;
                }

                _unCurentPlayer.money -= _money;
                _curentPlayer.money += _moneyPlus;
                break;

            case TypeCard.SpecialCard:

                break;
        }

        playerMe.txtMoney.text = MyConstant.FormatToCurrency(playerMe.money, 999999) + " $";
        playerOpponent.player.txtMoney.text = MyConstant.FormatToCurrency(playerOpponent.player.money, 999999) + " $";
    }
    public void ChangeKey()
    {
        if (playerMe.isMyTurn)
        {
            playerMe.isMyTurn = false;
            playerMe.panelFlag.gameObject.SetActive(false);
            playerOpponent.player.isMyTurn = true;
            playerOpponent.player.panelFlag.gameObject.SetActive(true);
        }
        else
        {
            playerMe.isMyTurn = true;
            playerMe.panelFlag.gameObject.SetActive(true);
            playerOpponent.player.isMyTurn = false;
            playerOpponent.player.panelFlag.gameObject.SetActive(false);
        }
    }
    PlayerGroup GetCurrentPlayer(bool _isCurrentPlay)
    {
        return (playerMe.isMyTurn == _isCurrentPlay) ? playerMe : playerOpponent.player;
    }
    void ActionShoot(TypeShoot _typeShoot) {
        playerMe.typeShoot = _typeShoot;
        playerMe.playerAnim.SetActionShoot(_typeShoot);
        StartCoroutine(playerOpponent.Shoot());
    }
#region BtnEvt
    public void OnClickBtnScissor()
    {
        if (gameStatus == GameStatus.Wait)
        {
            ActionShoot(TypeShoot.Scissor);
            //playerMe.typeShoot = TypeShoot.Scissor;
            //playerMe.playerAnim.SetActionShoot(TypeShoot.Scissor);
            //playerOpponent.Shoot();
        }
    }

    public void OnClickBtnStone()
    {
        if (gameStatus == GameStatus.Wait)
        {
            ActionShoot(TypeShoot.Stone);
            //playerMe.typeShoot = TypeShoot.Stone;
            //playerOpponent.Shoot();
        }
    }

    public void OnClickBtnPaper()
    {
        if (gameStatus == GameStatus.Wait)
        {
            ActionShoot(TypeShoot.Paper);
            //playerMe.typeShoot = TypeShoot.Paper;
            //playerOpponent.Shoot();
        }
    }

#endregion
}