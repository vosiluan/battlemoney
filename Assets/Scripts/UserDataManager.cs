﻿#if USE_ENCODE_JSON
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
#endif

using Root.Default.Script.Base;
using Root.Default.Script.Model;
using UnityEngine;

namespace Root.Default.Script.Manager
{
    [DefaultExecutionOrder(2)]
    public class UserDataManager : BaseSingleton<UserDataManager>
    {
        public UserDataSave userDataSave;

#region MonoBehaviour

        protected override void Awake()
        {
            base.Awake();

            userDataSave = GetData();

            if (userDataSave == null)
            {
                ResetData();
            }
        }

#endregion

#region Public

        public void SetVibration()
        {
            userDataSave.vibration = !userDataSave.vibration;

            SaveData();
        }

        public void LevelUp()
        {
            userDataSave.level++;

            SaveData();
        }

        public void AddGold(float value)
        {
            userDataSave.gold += value;

            SaveData();
        }

        public void AddDiamond(float value)
        {
            userDataSave.diamond += value;

            SaveData();
        }

        public void SetRemovedAds()
        {
            userDataSave.removedAds = true;

            SaveData();
        }

        public void SaveMusicVolume(float volume)
        {
            userDataSave.musicVolume = volume;

            SaveData();
        }

        public void SaveSoundVolume(float volume)
        {
            userDataSave.soundVolume = volume;

            SaveData();
        }

        public bool HasMusic => userDataSave.musicVolume > 0f;
        public bool HasSound => userDataSave.soundVolume > 0f;

#endregion

#region Private

        private UserDataSave GetData()
        {
            var json = PlayerPrefs.GetString("SAVE_DATA");

            //Log.Debug($"LOAD: {json}");

#if USE_ENCODE_JSON
            json = Decrypt(json);
#endif

            return JsonUtility.FromJson<UserDataSave>(json);
        }

#if USE_ENCODE_JSON
        private static string Encrypt(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            var keyBytes = new Rfc2898DeriveBytes(Const.PASSWORD_HASH,
                Encoding.ASCII.GetBytes(Const.SALT_KEY)).GetBytes(256 / 8);

            var symmetricKey = new RijndaelManaged
                               {
                                   Mode = CipherMode.CBC,
                                   Padding = PaddingMode.Zeros
                               };

            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(Const.VI_KEY));
            byte[] cipherTextBytes;

            using var memoryStream = new MemoryStream();

            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
            {
                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                cryptoStream.FlushFinalBlock();

                cipherTextBytes = memoryStream.ToArray();

                cryptoStream.Close();
            }

            memoryStream.Close();
            return Convert.ToBase64String(cipherTextBytes);
        }

        private static string Decrypt(string encryptedText)
        {
            var cipherTextBytes = Convert.FromBase64String(encryptedText);

            var keyBytes = new Rfc2898DeriveBytes(Const.PASSWORD_HASH,
                Encoding.ASCII.GetBytes(Const.SALT_KEY)).GetBytes(256 / 8);

            var symmetricKey = new RijndaelManaged
                               {
                                   Mode = CipherMode.CBC,
                                   Padding = PaddingMode.None
                               };

            var decrypt = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(Const.VI_KEY));
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decrypt, CryptoStreamMode.Read);
            var plainTextBytes = new byte[cipherTextBytes.Length];
            var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        }
#endif

        private void SaveData()
        {
            var json = JsonUtility.ToJson(userDataSave);

            //Log.Debug($"SAVE: {json}");

#if USE_ENCODE_JSON
            json = Encrypt(json);
#endif

            PlayerPrefs.SetString("SAVE_DATA", json);
            PlayerPrefs.Save();
        }

        [ContextMenu("Reset data")]
        private void ResetData()
        {
            userDataSave = new UserDataSave
                           {
                               level = 0,
                               diamond = 0f,
                               gold = 1000f,
                               musicVolume = 1f,
                               soundVolume = 1f,
                               vibration = true,
                               removedAds = false
                           };

            PlayerPrefs.DeleteAll();
            SaveData();
        }

#endregion

#region Protected

#endregion
    }
}