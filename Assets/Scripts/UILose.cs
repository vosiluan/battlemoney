﻿using System.Collections;
using System.Collections.Generic;
using Root.Default.Script.Manager;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UILose : IPopupController
{
    public Button btnComplete;

    private void Awake()
    {
        //AudioManager.Instance.PlayMusic("");
        //AudioManager.Instance.PlaySound("");
        //AudioManager.PlayVibrate();
        //btnComplete.onClick.AddListener(() => SceneManager.LoadScene(SceneManager.GetActiveScene().name));
    }
    public void OnBtnTabCLick(){
        IngameManager.instance.ClearTable();
        IngameManager.instance.InitForNewGame();
        gameObject.SetActive(false);
    }
}
