﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Global_Enum;
public class AIPlayer : MonoBehaviour
{
    public IngameManager.PlayerGroup player;
    public ResultShoot resultShoot;
    int countShoot;
    IEnumerator actionStartAI;
    public void InitAI() {
        countShoot = 0;
        resultShoot = ResultShoot.none;
        SetUpShoot(); // Set shoot trước khi start 1 level;
    }
    public void SetUpShoot(){
        int _randomNumber = Random.Range(1,101);// random giá trị từ 1 ---> 100.
        Debug.LogError("Random: "+ _randomNumber);
        resultShoot = (_randomNumber < 51) ? ResultShoot.win :
                           (_randomNumber > 50 && _randomNumber < 71) ? ResultShoot.lose :
                           (_randomNumber > 70 && _randomNumber < 86) ? ResultShoot.drawToWin :
                           (_randomNumber > 85 && _randomNumber < 101) ? ResultShoot.drawToLose : ResultShoot.none;
        countShoot = (resultShoot == ResultShoot.drawToLose || resultShoot == ResultShoot.drawToWin) ? 2 : 1;
    }
    public IEnumerator Shoot() {
        if (countShoot > 0)
        {
            switch (resultShoot)
            {
                case ResultShoot.win:
                    player.typeShoot = (IngameManager.instance.playerMe.typeShoot == TypeShoot.Scissor) ? TypeShoot.Stone :
                                        (IngameManager.instance.playerMe.typeShoot == TypeShoot.Paper) ? TypeShoot.Scissor :
                                        (IngameManager.instance.playerMe.typeShoot == TypeShoot.Stone) ? TypeShoot.Paper : TypeShoot.None;
                    break;
                case ResultShoot.lose:
                    player.typeShoot = (IngameManager.instance.playerMe.typeShoot == TypeShoot.Scissor) ? TypeShoot.Paper :
                                        (IngameManager.instance.playerMe.typeShoot == TypeShoot.Paper) ? TypeShoot.Stone :
                                        (IngameManager.instance.playerMe.typeShoot == TypeShoot.Stone) ? TypeShoot.Scissor : TypeShoot.None;
                    break;
                case ResultShoot.drawToLose:
                    if (countShoot == 1)
                        player.typeShoot = (IngameManager.instance.playerMe.typeShoot == TypeShoot.Scissor) ? TypeShoot.Paper :
                                        (IngameManager.instance.playerMe.typeShoot == TypeShoot.Paper) ? TypeShoot.Stone :
                                        (IngameManager.instance.playerMe.typeShoot == TypeShoot.Stone) ? TypeShoot.Scissor : TypeShoot.None;
                    else
                        player.typeShoot = IngameManager.instance.playerMe.typeShoot;
                    break;
                case ResultShoot.drawToWin:
                    if (countShoot == 1)
                        player.typeShoot = (IngameManager.instance.playerMe.typeShoot == TypeShoot.Scissor) ? TypeShoot.Stone :
                                        (IngameManager.instance.playerMe.typeShoot == TypeShoot.Paper) ? TypeShoot.Scissor :
                                        (IngameManager.instance.playerMe.typeShoot == TypeShoot.Stone) ? TypeShoot.Paper : TypeShoot.None;
                    else
                        player.typeShoot = IngameManager.instance.playerMe.typeShoot;
                    break;
            }
            countShoot--;
            player.playerAnim.SetActionShoot(player.typeShoot);
            if (countShoot ==0) {
                yield return Yielders.Get(1f);
                StartCoroutine(IngameManager.instance.CompareShoot());
            }
        }
    }
    public void StartAI() {
        StopAI();
        actionStartAI = AIHandle();
        StartCoroutine(actionStartAI);
    }
    public void StopAI() {
        if (actionStartAI != null) {
            StopCoroutine(actionStartAI);
            actionStartAI = null;
        }
    }
    IEnumerator AIHandle() {
        int _numberRandom = 0;
        while (true) {
            yield return Yielders.Get(2f);
            if (player.isMyTurn) {
                _numberRandom = Random.Range(1, 101);
                if (_numberRandom < 71)
                { /*Chọn best value*/
                    List<CardController> _listCard = IngameManager.instance.listCardsIngame;
                    long _moneyPlus = 0;
                    long _moneyMinus = 0;
                    long _temp = 0;
                    int _indexPlus = 0;
                    int _indexMinus = 0;
                    for (int i = 0; i < _listCard.Count; i++)
                    {
                        if (_listCard[i].cardInfo.isCanChoose)
                        {
                            if (_listCard[i].cardInfo.typeCard == TypeCard.BlueCard)
                            {
                                _temp = (_listCard[i].cardInfo.propertyCard == PropertyCard.bluePlus) ? _listCard[i].cardInfo.value :
                                                                 (_listCard[i].cardInfo.propertyCard == PropertyCard.bluePlusPercent) ? (player.money * _listCard[i].cardInfo.value) / 100 :
                                                                 (_listCard[i].cardInfo.propertyCard == PropertyCard.blueMultiply) ? (player.money * (_listCard[i].cardInfo.value - 1)) : 0;
                                if (_temp > _moneyPlus)
                                {
                                    _moneyPlus = _temp;
                                    _indexPlus = i;
                                }
                            }
                            else if (_listCard[i].cardInfo.typeCard == TypeCard.RedCard)
                            {
                                long _playerMoney = IngameManager.instance.playerMe.money;
                                _temp = (_listCard[i].cardInfo.propertyCard == PropertyCard.redDivide) ? _playerMoney - (_playerMoney / _listCard[i].cardInfo.value) :
                                        (_listCard[i].cardInfo.propertyCard == PropertyCard.redMinusPercent) ? (_playerMoney * _listCard[i].cardInfo.value) / 100 :
                                         (_listCard[i].cardInfo.propertyCard == PropertyCard.redSteal) ? ((_playerMoney * _listCard[i].cardInfo.value) / 100) * 2 : 0;
                                if (_temp > _moneyMinus)
                                {
                                    _moneyMinus = _temp;
                                    _indexMinus = i;
                                }
                            }
                        }
                    }
                    if (_moneyMinus > _moneyPlus)
                    {
                        player.playerAnim.SetActionPickCard();
                        yield return Yielders.Get(1f);
                        StartCoroutine(IngameManager.instance.listCardsIngame[_indexMinus].AnimationCardMove((IngameManager.instance.listCardsIngame[_indexMinus].cardInfo.typeCard == TypeCard.RedCard)?IngameManager.instance.playerMe:player, () =>
                        {
                            IngameManager.instance.HandleChooseCard(IngameManager.instance.listCardsIngame[_indexMinus].cardInfo);
                            IngameManager.instance.RemoveCardInStack(IngameManager.instance.listCardsIngame[_indexMinus]);
                            Destroy(IngameManager.instance.listCardsIngame[_indexMinus].gameObject);
                            IngameManager.instance.listCardsIngame.RemoveAt(_indexMinus);
                            IngameManager.instance.ChangeKey();
                        }));
                    }
                    else if (_moneyMinus < _moneyPlus)
                    {
                        player.playerAnim.SetActionPickCard();
                        yield return Yielders.Get(1f);
                        StartCoroutine(IngameManager.instance.listCardsIngame[_indexPlus].AnimationCardMove((IngameManager.instance.listCardsIngame[_indexPlus].cardInfo.typeCard == TypeCard.RedCard) ? IngameManager.instance.playerMe : player, () =>
                        {
                            IngameManager.instance.HandleChooseCard(IngameManager.instance.listCardsIngame[_indexPlus].cardInfo);
                            IngameManager.instance.RemoveCardInStack(IngameManager.instance.listCardsIngame[_indexPlus]);
                            Destroy(IngameManager.instance.listCardsIngame[_indexPlus].gameObject);
                            IngameManager.instance.listCardsIngame.RemoveAt(_indexPlus);
                            IngameManager.instance.ChangeKey();
                        }));
                    }
                }
                else
                {
                    /*Chọn random card*/
                    List<CardController> _listCardCanChoose = new List<CardController>();
                    for (int i = 0; i < IngameManager.instance.listCardsIngame.Count; i++)
                    {
                        if (IngameManager.instance.listCardsIngame[i].cardInfo.isCanChoose)
                        {
                            _listCardCanChoose.Add(IngameManager.instance.listCardsIngame[i]);
                        }
                    }
                    int _indexCardChoose = Random.Range(0, _listCardCanChoose.Count);
                    player.playerAnim.SetActionPickCard();
                    yield return Yielders.Get(1f);
                    StartCoroutine(IngameManager.instance.listCardsIngame[_indexCardChoose].AnimationCardMove((IngameManager.instance.listCardsIngame[_indexCardChoose].cardInfo.typeCard == TypeCard.RedCard) ? IngameManager.instance.playerMe : player, () =>
                    {
                        IngameManager.instance.HandleChooseCard(IngameManager.instance.listCardsIngame[_indexCardChoose].cardInfo);
                        IngameManager.instance.RemoveCardInStack(IngameManager.instance.listCardsIngame[_indexCardChoose]);
                        Destroy(IngameManager.instance.listCardsIngame[_indexCardChoose].gameObject);
                        IngameManager.instance.listCardsIngame.RemoveAt(_indexCardChoose);
                        IngameManager.instance.ChangeKey();
                    }));
                    
                }
            }
            yield return null;
        }
    }
}
