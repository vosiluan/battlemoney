﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Object = UnityEngine.Object;
using SRandom = System.Random;
using URandom = UnityEngine.Random;

namespace Root.Default.Script
{
    public enum CornerDirection
    {
        BottomLeft,
        TopLeft,
        TopRight,
        BottomRight,
        BottomCenter,
        LeftCenter,
        TopCenter,
        RightCenter,
        Center
    }

    [Serializable]
    public class CornerData
    {
        public CornerDirection cornerDirection;
        public Vector3 value;
    }

    public static class App
    {
        private static List<CornerData> _corners = new List<CornerData>();

        private static Camera _camera;

#region Rect Transform

#if USE_APP_RECT_TRANSFORM
        public static void SetLeft(this RectTransform rt, float left)
        {
            rt.offsetMin = new Vector2(left, rt.offsetMin.y);
        }

        public static void SetRight(this RectTransform rt, float right)
        {
            rt.offsetMax = new Vector2(-right, rt.offsetMax.y);
        }

        public static void SetTop(this RectTransform rt, float top)
        {
            rt.offsetMax = new Vector2(rt.offsetMax.x, -top);
        }

        public static void SetBottom(this RectTransform rt, float bottom)
        {
            rt.offsetMin = new Vector2(rt.offsetMin.x, bottom);
        }
#endif

#endregion

#region Tween

#if USE_APP_TWEEN
        public static TweenerCore<string, string, StringOptions> DoText(this TMP_Text target, string endValue,
            float duration, bool richTextEnabled = true, ScrambleMode scrambleMode = ScrambleMode.None,
            string scrambleChars = null)
        {
            if (endValue == null)
            {
                Log.DebuggerPriority(
                    "You can't pass a NULL string to DOText: an empty string will be used instead to avoid errors");

                endValue = string.Empty;
            }

            var t = DOTween.To(() => target.text, x => target.text = x, endValue, duration);

            t.SetOptions(richTextEnabled, scrambleMode, scrambleChars).SetTarget(target);
            return t;
        }

        public static Tween DoCounter(float start, float end, float duration, Action<float> onChange)
        {
            return DOTween.To(() => start, x => onChange(x), end, duration);
        }
#endif

#endregion

#region Camera

        public static Camera Camera
        {
            get
            {
                if (_camera == null ||
                    _camera.gameObject == null ||
                    !_camera.gameObject.activeInHierarchy)
                {
                    _camera = Object.FindObjectOfType<Camera>();
                }

                return _camera;
            }
        }

#endregion

#region Enum


#endregion

#region List

        public static List<CornerData> GetWorldCorner(this RectTransform rt)
        {
            var v4 = new Vector3[4];

            _corners.Clear();
            rt.GetWorldCorners(v4);

            var bottomCenter = (v4[0] + v4[3]) / 2f;
            var topCenter = (v4[1] + v4[2]) / 2f;
            var leftCenter = (v4[0] + v4[1]) / 2f;
            var rightCenter = (v4[2] + v4[3]) / 2f;
            var center = (leftCenter + rightCenter) / 2f;

            // All direction corner rect transform
            // v4[1]-Top Left     |    Top Center    |    v4[2]-Top Right
            //      -
            // Left Center        |      Center      |     Right Center
            //      -
            // v4[0]-Bottom Left  |   Bottom Center  |    v4[3]-Bottom Right

            _corners =
                new List<CornerData>
                {
                    new CornerData
                    {
                        cornerDirection = CornerDirection.BottomLeft,
                        value = v4[0]
                    },
                    new CornerData
                    {
                        cornerDirection = CornerDirection.LeftCenter,
                        value = leftCenter
                    },
                    new CornerData
                    {
                        cornerDirection = CornerDirection.TopLeft,
                        value = v4[1]
                    },
                    new CornerData
                    {
                        cornerDirection = CornerDirection.TopCenter,
                        value = topCenter
                    },
                    new CornerData
                    {
                        cornerDirection = CornerDirection.TopRight,
                        value = v4[2]
                    },
                    new CornerData
                    {
                        cornerDirection = CornerDirection.RightCenter,
                        value = rightCenter
                    },
                    new CornerData
                    {
                        cornerDirection = CornerDirection.BottomRight,
                        value = v4[3]
                    },
                    new CornerData
                    {
                        cornerDirection = CornerDirection.BottomCenter,
                        value = bottomCenter
                    },
                    new CornerData
                    {
                        cornerDirection = CornerDirection.Center,
                        value = center
                    }
                };

            return _corners;
        }

#if UNITY_EDITOR
        public static IEnumerable<object> LoadAllAssetsAtPath(string pathFolder)
        {
            const string ASSET = "Assets\\";

            if (pathFolder.StartsWith(ASSET))
            {
                pathFolder = pathFolder.Remove(0, ASSET.Length);
            }

            return Directory.GetFiles($"{Application.dataPath}/{pathFolder}")
                .Select(file => AssetDatabase.LoadAssetAtPath(
                            file.Substring(Application.dataPath.Length - ASSET.Length + 1), typeof(object)))
                .Where(obj => obj != null)
                .Cast<object>().ToList();
        }
#endif

        public static IList<T> Swap<T>(this IList<T> list, int start, int end)
        {
            (list[start], list[end]) = (list[end], list[start]);
            return list;
        }

#endregion

#region Void

        public static void IgnoreRaycast(this Component c)
        {
            c.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
        }

        public static void SetLayer(this GameObject self, int layer, bool includeChildren = true)
        {
            self.layer = layer;

            if (!includeChildren)
            {
                return;
            }

            var children = self.GetComponentsInChildren<Transform>(true);

            foreach (var x in children)
            {
                x.gameObject.layer = layer;
            }
        }

        public static void Insert<TKey, TValue>(this IDictionary<TKey, TValue> dt, int index, TKey key, TValue value)
        {
            dt.Keys.ToList().Insert(index, key);
            dt.Values.ToList().Insert(index, value);
        }

        /// <summary>
        ///     Replace oldKey to newKey
        /// </summary>
        /// <param name="dt">dictionary need replace</param>
        /// <param name="oldKey">old key need to replace by new key</param>
        /// <param name="newKey">new key need to replace to old key</param>
        /// <param name="newValue">new value need to replace to old value of old key</param>
        public static void Replace<TKey, TValue>(this IDictionary<TKey, TValue> dt,
            TKey oldKey, TKey newKey, TValue newValue)
        {
            var index = dt.Keys.ToList().IndexOf(oldKey);

            dt.Insert(index, newKey, newValue);
            /*dt.Remove(oldKey);*/
        }

        public static void LimitCamera(this Transform t)
        {
            var p = Camera.WorldToViewportPoint(t.position);

            p.x = Mathf.Clamp01(p.x);
            p.y = Mathf.Clamp01(p.y);

            t.position = Camera.ViewportToWorldPoint(p);
        }

        /// <summary>
        ///     Keep the ui inside the camera eg tooltip
        ///     BL is bottom left
        ///     TR is top right
        /// </summary>
        /// <param name="rt">ui need to keep inside camera</param>
        /// <param name="canvasParent">canvas contains the ui to keep inside</param>
        /// <param name="newPosition">new position the tooltip can place</param>
        public static void LimitCamera(this RectTransform rt, RectTransform canvasParent, Vector3? newPosition = null)
        {
            var v4 = new Vector3[4];

            if (newPosition != null)
            {
                rt.position = (Vector3) newPosition;
            }

            canvasParent.GetWorldCorners(v4);

            var canvasBL = v4[0];
            var canvasTR = v4[2];
            var canvasSize = canvasTR - canvasBL;

            rt.GetWorldCorners(v4);

            var rectBL = v4[0];
            var rectTR = v4[2];
            var rectSize = rectTR - rectBL;

            var position = rt.position;
            var deltaBL = position - rectBL;
            var deltaTR = rectTR - position;

            position.x = rectSize.x < canvasSize.x
                             ? Mathf.Clamp(position.x, canvasBL.x + deltaBL.x, canvasTR.x - deltaTR.x)
                             : Mathf.Clamp(position.x, canvasTR.x - deltaTR.x, canvasBL.x + deltaBL.x);

            position.y = rectSize.y < canvasSize.y
                             ? Mathf.Clamp(position.y, canvasBL.y + deltaBL.y, canvasTR.y - deltaTR.y)
                             : Mathf.Clamp(position.y, canvasTR.y - deltaTR.y, canvasBL.y + deltaBL.y);

            rt.position = position;
        }

#endregion

#region Vector3

        /// <summary>
        ///     Auto calculate to change scale fit camera
        /// </summary>
        /// <param name="t"></param>
        /// <param name="keepScaleZ">transform need keep scale</param>
        /// <returns></returns>
        public static void ResizeToScreen(this Transform t, bool keepScaleZ = false)
        {
            var height = Camera.orthographic
                             ? Camera.orthographicSize * 2f
                             : Camera.fieldOfView * 5f;

            var width = height / Screen.height * Screen.width;
            var z = t.localScale.z;

            t.localScale = new Vector3(width, height, keepScaleZ ? z : width);
        }

        public static void ResizeSprite2DToScreen(this Transform t, SpriteRenderer sr, bool keepScaleZ = false)
        {
            var bound = sr.sprite.bounds;

            t.ResizeToScreen(keepScaleZ);

            var scale = t.localScale;
            var z = scale.z;

            scale.x /= bound.size.x;
            scale.y /= bound.size.y;

            t.localScale = new Vector3(scale.x, scale.y, keepScaleZ ? z : scale.x);
        }

        public static Vector3 Random(this Bounds bounds)
        {
            var min = bounds.min;
            var max = bounds.max;

            return new Vector3(URandom.Range(min.x, max.x),
                URandom.Range(min.y, max.y),
                URandom.Range(min.z, max.z));
        }

        public static Vector3 Platform
        {
            get
            {
#if UNITY_EDITOR || UNITY_WEBGL || UNITY_STANDALONE_WIN || UNITY_WSA
                return Input.mousePosition;
#elif UNITY_ANDROID || UNITY_IOS
                if (Input.touchCount > 0)
                {
                    return Input.GetTouch(0).position;
                }
                
                return Vector3.zero;
#else
                return Vector3.zero;
#endif
            }
        }

        public static Vector3 UnwrapAngle(Vector3 angle)
        {
            if (angle.x >= 0f &&
                angle.y >= 0f &&
                angle.z >= 0f)
            {
                return angle;
            }

            angle.x = -angle.x % 360;
            angle.y = -angle.y % 360;
            angle.z = -angle.z % 360;
            return 360f * Vector3.one - angle;
        }

        public static Vector3 WrapAngle(Vector3 angle)
        {
            angle.x %= 360f;
            angle.y %= 360f;
            angle.z %= 360f;

            if (angle.x > 180f)
            {
                angle.x -= 360f;
            }

            if (angle.y > 180f)
            {
                angle.y -= 360f;
            }

            if (angle.z > 180f)
            {
                angle.z -= 360f;
            }

            return angle;
        }

        public static Vector3 Abs(Vector3 v)
        {
            return new Vector3(Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z));
        }

        public static Vector3 CalculateVelocity(Vector3 target, Vector3 origin, float time = 1f)
        {
            var distance = target - origin;
            var vxz = distance.magnitude / time;
            var vy = distance.y / time + 0.5f * Mathf.Abs(Physics.gravity.y) * time;
            var velocity = distance.normalized;

            velocity *= vxz;
            velocity.y = vy;
            return velocity;
        }

        public static Vector3[] CalculatePoints(Vector3 target, Vector3 origin, int numberPoint = 5)
        {
            var points = new Vector3[numberPoint];

            for (var i = 0; i < numberPoint; i++)
            {
                points[i] = CalculatePositionInTime(target, origin, i / (float) numberPoint);
            }

            return points;
        }

        private static Vector3 CalculatePositionInTime(Vector3 target, Vector3 origin, float time)
        {
            var result = target + origin * time;
            var sy = -0.5f * Mathf.Abs(Physics.gravity.y) * (time * time) + origin.y * time + target.y;

            result.y = sy;
            return result;
        }

#endregion

#region Int

        public static int? ConvertLayerIndex(this int layer)
        {
            for (var index = 0; index < sizeof(int) * 8; index++)
            {
                if ((layer & 1) == 1)
                {
                    return index;
                }

                layer >>= 1;
            }

            return null;
        }

        public static int DropChance(this List<float> drops)
        {
            var range = drops.Sum();
            var rnd = URandom.Range(0, range);

            var maxDrop = 0f;
            var index = 0;
            var top = 0f;

            for (var i = 0; i < drops.Count; i++)
            {
                var item = drops[i];

                if (maxDrop < item)
                {
                    maxDrop = item;
                    index = i;
                }

                top += item;

                if (rnd < top)
                {
                    return i;
                }
            }

            return index;
        }

#endregion

#region Float

        public static float WrapAngle(float angle)
        {
            angle %= 360f;

            if (angle > 180f)
            {
                angle -= 360f;
            }

            return angle;
        }

        public static float UnwrapAngle(float angle)
        {
            if (angle >= 0)
            {
                return angle;
            }

            angle = -angle % 360;
            return 360 - angle;
        }

        public static float BoundsContainedPercentage(this Bounds origin, Bounds target)
        {
            var total = 1f;

            for (var i = 0; i < 3; i++)
            {
                var dist = target.min[i] > origin.center[i]
                               ? target.max[i] - origin.max[i]
                               : origin.min[i] - target.min[i];

                total *= Mathf.Clamp01(1f - dist / target.size[i]);
            }

            return total;
        }

        public static float NextFloat(this SRandom rnd, float max, float min)
        {
            return min > max
                       ? (float) rnd.NextDouble() * (min - max) + max
                       : (float) rnd.NextDouble() * (max - min) + min;
        }

        public static float Abs(float a, float b)
        {
            return a > b ? a - b : b - a;
        }

#endregion

#region Double

        public static double Abs(double a, double b)
        {
            return a > b ? a - b : b - a;
        }

        public static double NextDouble(this SRandom rnd, double max, double min)
        {
            return min > max
                       ? rnd.NextDouble() * (min - max) + max
                       : rnd.NextDouble() * (max - min) + min;
        }

#endregion

#region String

        public static string NewLine => "\u000a";

        public static string RemoveAllWhiteSpace(this string s)
        {
            return Regex.Replace(s, @"\s+", "");
        }

        public static string AddWhiteSpaceFrontCharUpper(this string s)
        {
            for (var i = 0; i < s.Length; i++)
            {
                if (i == 0)
                {
                    if (char.IsLower(s[0]))
                    {
                        s = s.Replace(s[0], char.ToUpper(s[0]));
                    }
                }
                else if (char.IsUpper(s[i]) &&
                         !char.IsWhiteSpace(s[i - 1]))
                {
                    s = s.Insert(i, " ");
                }
                else if (char.IsLower(s[i]) &&
                         char.IsWhiteSpace(s[i - 1]))
                {
                    s = s.Replace(s[i], char.ToUpper(s[i]));
                }
            }

            return s;
        }

        public static string RemoveExtraSpace(this string s)
        {
            s = Regex.Replace(s, @"\s+", " ");

            if (s.Length > 0 &&
                char.IsWhiteSpace(s[0]))
            {
                s = s.Remove(0, 1);
            }

            return s;
        }

        public static string Minimum10(this float value)
        {
            return value < 10 ? $"0{value}" : $"{value}";
        }

        public static string Minimum10(this int value)
        {
            return value < 10 ? $"0{value}" : $"{value}";
        }

        public static string Minimum10(this double value)
        {
            return value < 10 ? $"0{value}" : $"{value}";
        }

        public static string Match(this string s, char c1, char c2 = '\0', bool firstLast = false)
        {
            var begin = firstLast ? s.LastIndexOf(c1) : s.IndexOf(c1);
            var end = c2 == '\0' ? s.Length : s.LastIndexOf(c2);
            return s.Substring(begin + 1, end - begin - 1);
        }

        public static string RandomString(int min = 1, int max = 5)
        {
            var hash = "78128736812673jhasgdjhagsjdznmxcvnvnb12873";
            if (max >= hash.Length - 1)
            {
                max = hash.Length - 1;
            }

            var chars = URandom.Range(min, max);
            var result = $"{hash[chars]}";

            for (var i = 0; i < chars; i++)
            {
                result += hash[URandom.Range(0, hash.Length)];
            }

            return result;
        }

#endregion

#region Bool

        public static bool IsAsync(this Action action)
        {
            return action.Method.IsDefined(typeof(AsyncStateMachineAttribute), false);
        }

        public static bool IsVisibleCamera(this Renderer renderer)
        {
            return GeometryUtility
                .TestPlanesAABB(GeometryUtility.CalculateFrustumPlanes(Camera), renderer.bounds);
        }

        public static bool IsPointerOverUIObject()
        {
            var ped = new PointerEventData(EventSystem.current)
                      {
                          position = Platform
                      };

            var results = new List<RaycastResult>();

            EventSystem.current.RaycastAll(ped, results);
            return results.Count > 0;
        }

        public static bool IsEnglishLetter(this char c)
        {
            return c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z';
        }

        /*public static bool Equal(float a, float b, float epsilon = 0f)
        {
            return Mathf.Abs(a) - Mathf.Abs(b) <= (epsilon == 0f ? Const.EPSILON_MIN : epsilon);
        }

        public static bool Equal(Vector3 a, Vector3 b, float epsilon = 0f)
        {
            return Equal(a.x, b.x, epsilon) && Equal(a.y, b.y, epsilon) && Equal(a.z, b.z, epsilon);
        }

        public static bool RandomBool => URandom.value > 0.5f;

        public static bool Distance(Vector3 a, Vector3 b, float distance)
        {
            return (a - b).sqrMagnitude <= distance * distance;
        }*/

#endregion

#region Scroll Rect

        public static void ScrollTo(this ScrollRect sr, RectTransform target, float duration = 1f)
        {
            var srRt = sr.transform as RectTransform;

            if (srRt == null)
            {
                return;
            }

            // Item is here
            var itemCenterPositionInScroll = srRt.InverseTransformPoint(GetWidgetWorldPoint(target));

            // But must be here
            var targetPositionInScroll = srRt.InverseTransformPoint(GetWidgetWorldPoint(sr.viewport));

            // So it has to move this distance
            var difference = targetPositionInScroll - itemCenterPositionInScroll;

            difference.z = 0f;

            // clear axis data that is not enabled in the scroll rect
            if (!sr.horizontal)
            {
                difference.x = 0f;
            }

            if (!sr.vertical)
            {
                difference.y = 0f;
            }

            var rect = sr.content.rect;
            var rect1 = srRt.rect;

            var normalizedDifference = new Vector2(difference.x / (rect.size.x - rect1.size.x),
                difference.y / (rect.size.y - rect1.size.y));

            var newNormalizedPosition = sr.normalizedPosition - normalizedDifference;

            if (sr.movementType != ScrollRect.MovementType.Unrestricted)
            {
                newNormalizedPosition.x = Mathf.Clamp01(newNormalizedPosition.x);
                newNormalizedPosition.y = Mathf.Clamp01(newNormalizedPosition.y);
            }

            /*
            sr.DOKill();
            sr.DONormalizedPos(newNormalizedPosition, duration);*/
        }

        private static Vector3 GetWidgetWorldPoint(RectTransform target)
        {
            // pivot position + item size has to be included
            var pivotOffset = new Vector3((0.5f - target.pivot.x) * target.rect.size.x,
                (0.5f - target.pivot.y) * target.rect.size.y, 0f);

            return target.parent.TransformPoint(target.localPosition + pivotOffset);
        }

#endregion

#region Define

#if UNITY_EDITOR
        public static void AddDefine(params string[] define)
        {
            var availableDefines = PlayerSettings
                .GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);

            var split = new List<string>(availableDefines.Split(';'));

            foreach (var x in define)
            {
                if (!split.Contains(x))
                {
                    split.Add(x);
                }
            }

            _ApplyDefine(string.Join(";", split.ToArray()));
        }

        public static void RemoveDefine(params string[] define)
        {
            var availableDefines = PlayerSettings
                .GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);

            var split = new List<string>(availableDefines.Split(';'));

            foreach (var x in define)
            {
                if (split.Contains(x))
                {
                    split.Remove(x);
                }
            }

            _ApplyDefine(string.Join(";", split.ToArray()));
        }

        private static void _ApplyDefine(string define)
        {
            PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, define);
        }
#endif

#endregion

#region UniTask

        /*public static UniTask<T> AsAsync<T>(this Action<Action<T>> target)
        {
            var tcs = new UniTaskCompletionSource<T>();

            try
            {
                target(t => tcs.TrySetResult(t));
            }
            catch (Exception ex)
            {
                tcs.TrySetException(ex);
            }

            return tcs.Task;
        }

        public static async UniTask InvokeAsync(this Action action)
        {
#if UNITY_WEBGL
            var execute = new Func<UniTask>
            (async () =>
             {
                 action();
                 await UniTask.Yield();
             });

            await UniTask.WhenAll(execute());
#else
            await UniTask.RunOnThreadPool(action);
#endif
        }*/

#endregion
    }
}