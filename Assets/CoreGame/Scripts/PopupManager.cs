﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Lean.Pool;
using NaughtyAttributes;

public class PopupManager : MonoBehaviour {
	public static PopupManager Instance{
		get{
			if(ins == null){
				ins = Instantiate(CoreGameManager.instance.singletonPrefabInfo.popupManagerPrefab);
				DontDestroyOnLoad (ins.gameObject);
			}
			return ins;
		}
	}
    static PopupManager ins;

	[SerializeField] Canvas myCanvas;
    [SerializeField] CanvasGroup popupCanvasGroup;
	[SerializeField] Transform popupContainer;
	[SerializeField] CanvasGroup notificationCanvasGroup;
	[SerializeField] Transform notificationContainer;
	[SerializeField] Transform pool;
	[SerializeField] UISetting popupSettingPrefab;
    [Header("Prefabs")]
	List<IPopupController> popupsActive;
	void Awake() {
		if (ins != null && ins != this) { 
			Destroy(this.gameObject);
			return;
		}
		popupCanvasGroup.alpha = 1f;
		popupCanvasGroup.blocksRaycasts = false;
		if(popupsActive == null){
			popupsActive = new List<IPopupController>();
		}
	}
    /**
	 * AddPopupActive : add các popup đang active vào list
	 * */
	void AddPopupActive(IPopupController _popup){
		_popup.transform.SetAsLastSibling();
		popupsActive.Add(_popup);
	}
	/**
	 * RemovePopupActive : remove các popup đang active ra khỏi list
	 * */
	public void RemovePopupActive(IPopupController _popup){
		if(popupsActive.Count == 0){
			return;
		}
		popupsActive.Remove(_popup);
		_popup.SelfDestruction();
		if (popupsActive.Count == 0) {
			popupCanvasGroup.alpha = 0f;
			popupCanvasGroup.blocksRaycasts = false;
		}
	}
	public void UnActiveAllPopups(){
		if(popupsActive != null && popupsActive.Count > 0){
			for(int i = 0; i < popupsActive.Count; i++){
				popupsActive[i].SelfDestruction();
				popupsActive.RemoveAt(i);
				i--;
			}
			popupsActive.Clear ();
		}
		popupCanvasGroup.alpha = 0f;
		popupCanvasGroup.blocksRaycasts = false;
	}
	public void DestroyAll(){
		UnActiveAllPopups();
		int _poolChildCount = pool.childCount;
		for (var i = _poolChildCount - 1; i >= 0; i--){
			Destroy(pool.GetChild(i).gameObject);
		}
	}
    public void SetCanvasPopupAgain(){ 
		if (myCanvas.worldCamera == null){
			myCanvas.worldCamera = Camera.main;
		}
		popupCanvasGroup.alpha = 1f;
		popupCanvasGroup.blocksRaycasts = true;
	}
	public UISetting CreatePopupSetting(){
		SetCanvasPopupAgain();

		UISetting _tmpPopup = LeanPool.Spawn(popupSettingPrefab, Vector3.zero, Quaternion.identity, popupContainer, pool);
		//_tmpPopup.Init(() =>{
		//	RemovePopupActive(_tmpPopup);
		//});
		// MyAudioManager.instance.PlaySfx(PopupManager.Instance.myInfoAudio.sfx_Popup);
		AddPopupActive(_tmpPopup);
		
		return _tmpPopup;
	}
	public PopupShopController CreaterPopupShop(PopupShopController _popupPrefab) {
		SetCanvasPopupAgain();

		PopupShopController _tmpPopup = LeanPool.Spawn(_popupPrefab, Vector3.zero, Quaternion.identity, popupContainer, pool);
		_tmpPopup.Init(() => {
			RemovePopupActive(_tmpPopup);
		});
		// MyAudioManager.instance.PlaySfx(PopupManager.Instance.myInfoAudio.sfx_Popup);
		AddPopupActive(_tmpPopup);
		return _tmpPopup;
	}
	#region For Test
	[Button] void TestDestroyAll(){
		UnActiveAllPopups();
		int _poolChildCount = pool.childCount;
		for (var i = _poolChildCount - 1; i >= 0; i--){
			Destroy(pool.GetChild(i).gameObject);
		}
	}
    #endregion
}