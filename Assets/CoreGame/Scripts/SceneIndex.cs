﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SceneIndex
{
    Home_Scene =0,
    Lobby_Scene = 1,
    LevelMap_Scene = 2,
    Unknown = 999
}
