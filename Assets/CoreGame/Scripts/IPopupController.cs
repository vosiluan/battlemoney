﻿using UnityEngine;
using System.Collections;
using Lean.Pool;

public class IPopupController : MySimplePoolObjectController {

	[SerializeField] protected CanvasGroup myCanvasGroup;
	[SerializeField] protected Transform mainContainer;
	public System.Action onClose;

	protected int idTweenCanvasGroup = -1;
	protected int idTweenMainContainer = -1;

	public override void ResetData(){
		onClose = null;
	}
	public override void StopAllActionNow(){
		base.StopAllActionNow();
		if(idTweenCanvasGroup != -1 && LeanTween.descr(idTweenCanvasGroup) != null){
			LeanTween.cancel(idTweenCanvasGroup);
		}
		idTweenCanvasGroup = -1;

		if(idTweenMainContainer != -1 && LeanTween.descr(idTweenMainContainer) != null){
			LeanTween.cancel(idTweenMainContainer);
		}
		idTweenMainContainer = -1;
	}

	public virtual void Show(System.Action _onFinished = null){
		if(idTweenCanvasGroup != -1 && LeanTween.descr(idTweenCanvasGroup) != null){
			LeanTween.cancel(idTweenCanvasGroup);
		}
		if(idTweenMainContainer != -1 && LeanTween.descr(idTweenMainContainer) != null){
			LeanTween.cancel(idTweenMainContainer);
		}
		
		transform.SetAsLastSibling();
		myCanvasGroup.alpha = 0f;
		myCanvasGroup.blocksRaycasts = true;
		
		idTweenCanvasGroup = LeanTween.alphaCanvas(myCanvasGroup, 1f, 0.2f).setEase(LeanTweenType.easeOutBack).setOnComplete(()=>{
			idTweenCanvasGroup = -1;
		}).setIgnoreTimeScale(true).id;
		mainContainer.localScale = Vector3.one * 0.6f;
        idTweenMainContainer = LeanTween.scale(mainContainer.gameObject, Vector3.one, 0.2f).setEase(LeanTweenType.easeInBack).setOnComplete(() =>
        {
            idTweenMainContainer = -1;
            if (_onFinished != null)
            {
                _onFinished();
            }
        }).setIgnoreTimeScale(true).id;
    }

	public virtual void Hide(System.Action _onFinished = null){
		if(idTweenCanvasGroup != -1 && LeanTween.descr(idTweenCanvasGroup) != null){
			LeanTween.cancel(idTweenCanvasGroup);
		}
		if(idTweenMainContainer != -1 && LeanTween.descr(idTweenMainContainer) != null){
			LeanTween.cancel(idTweenMainContainer);
		}

		myCanvasGroup.blocksRaycasts = false;
		idTweenCanvasGroup = LeanTween.alphaCanvas(myCanvasGroup, 0f, 0.2f).setOnComplete(()=>{
			idTweenCanvasGroup = -1;
		}).setEase(LeanTweenType.easeInBack).setIgnoreTimeScale(true).id;

		idTweenMainContainer = LeanTween.scale(mainContainer.gameObject, Vector3.one * 0.6f, 0.2f).setEase(LeanTweenType.easeInBack).setOnComplete(()=>{
			idTweenMainContainer = -1;
			if(_onFinished != null){
				_onFinished();
			}
		}).setIgnoreTimeScale(true).id;
	}
	public virtual void AddListenerClose(System.Action _callback){
		onClose += _callback;
	}
	public virtual void Close(){}
}
