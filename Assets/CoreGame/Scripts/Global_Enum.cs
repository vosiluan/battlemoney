﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global_Enum
{
    public enum PanelState {
        Hide = 0,
        Show = 1
    }
    public enum TypeCard { 
        BlueCard,
        RedCard,
        SpecialCard
    }
    public enum PropertyCard { 
        bluePlus,
        bluePlusPercent,
        blueMultiply,
        redMinusPercent,
        redSteal,
        redDivide,
        specialMoves,
        specialRandom
    }
    public enum LevelTypeCardSpecial{
        Random,
        SpecialMoves,
        SpecialRandom
    }
    public enum UnlockType { 
        UnlockCards,
        UnlockStack,
    }
    public enum TypeShoot {
        Scissor,
        Stone,
        Paper,
        None,
    }
    public enum ResultShoot { 
        none,
        win,
        lose,
        drawToWin,
        drawToLose
    }
    public enum GameStatus { 
        Wait,
        Start,
        Finish,
    }
}
