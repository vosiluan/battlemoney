//Maya ASCII 2022 scene
//Name: Card_Purple.ma
//Last modified: Mon, Jun 13, 2022 09:40:24 PM
//Codeset: 1252
requires maya "2022";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2022";
fileInfo "version" "2022";
fileInfo "cutIdentifier" "202102181415-29bfc1879c";
fileInfo "osv" "Windows 10 Pro v2009 (Build: 19043)";
fileInfo "UUID" "62DD3D06-4918-CF03-89AC-76AB3DAAF070";
createNode transform -n "CardBack_Purple";
	rename -uid "B0AE8682-4994-B0AB-A96E-818256F79A29";
	addAttr -is true -ci true -h true -k true -sn "MaxHandle" -ln "MaxHandle" -smn 
		0 -smx 0 -at "long";
	setAttr -k on ".MaxHandle" 60;
createNode mesh -n "CardBack_PurpleShape" -p "CardBack_Purple";
	rename -uid "F27DBF07-4905-5237-0448-198CE15DDF8A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50117117166519165 0.26066265162080526 ;
	setAttr ".uvst[0].uvsn" -type "string" "UVChannel_1";
	setAttr -s 32 ".uvst[0].uvsp[0:31]" -type "float2" 0.90706813 0.95110923
		 0.095353097 0.95110923 0.095353097 0.56170803 0.90706813 0.56170797 0.068477809 0.46821415
		 0.068477809 0.053024739 0.93394876 0.053024799 0.93394876 0.46821415 0.94895017 0.96874022
		 0.94898951 0.54383713 0.023695886 0.034056693 0.023737967 0.48709905 0.92877567 0.52411699
		 0.073609322 0.52426881 0.95704836 0.013192458 0.045248449 0.013030602 0.053392082
		 0.54399604 0.053352743 0.96889919 0.97864646 0.48726857 0.97860456 0.03422609 0.073566228
		 0.98861939 0.92873263 0.98846763 0.045294344 0.50813287 0.95709431 0.5082947 0.97873062
		 0.48718232 0.95717847 0.50820845 0.95713252 0.013106151 0.97868866 0.034139842 0.045378506
		 0.50804663 0.023822129 0.4870128 0.023780167 0.033970386 0.045332611 0.012944354;
	setAttr ".cuvs" -type "string" "UVChannel_1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr -s 12 ".pt";
	setAttr ".pt[4]" -type "float3" 0 -0.030234819 0 ;
	setAttr ".pt[5]" -type "float3" 0 -0.030234819 0 ;
	setAttr ".pt[6]" -type "float3" 0 -0.030234819 0 ;
	setAttr ".pt[7]" -type "float3" 0 -0.030234819 0 ;
	setAttr ".pt[12]" -type "float3" 0 -0.030234819 0 ;
	setAttr ".pt[13]" -type "float3" 0 -0.030234819 0 ;
	setAttr ".pt[14]" -type "float3" 0 -0.030234819 0 ;
	setAttr ".pt[15]" -type "float3" 0 -0.030234819 0 ;
	setAttr ".pt[20]" -type "float3" 0 -0.030234819 0 ;
	setAttr ".pt[21]" -type "float3" 0 -0.030234819 0 ;
	setAttr ".pt[22]" -type "float3" 0 -0.030234819 0 ;
	setAttr ".pt[23]" -type "float3" 0 -0.030234819 0 ;
	setAttr -s 24 ".vt[0:23]"  -6.22280121 0 -2.95233536 -6.22280121 0 2.95233536
		 5.94187927 0 -3.22642899 5.94187927 0 3.22642899 -6.22280121 0.067573547 -2.95233536
		 -5.94187546 0.067573547 3.22642899 5.94187927 0.067573547 -3.22642899 6.22280312 0.067573547 2.95233917
		 -5.94187546 0 -3.22642899 -5.94187546 0 3.22642899 6.22280121 0 -2.95233536 6.22280312 0 2.95233917
		 -5.94187546 0.067573547 -3.22642899 -6.22280121 0.067573547 2.95233536 6.22280121 0.067573547 -2.95233536
		 5.94187927 0.067573547 3.22642899 5.63996887 0.00040435791 -2.70566177 5.63996696 0.00040435791 2.70566177
		 -5.63996506 0.00040435791 2.70566177 -5.63996696 0.00040435791 -2.70566177 -5.63996506 0.067169189 2.70566177
		 5.63996887 0.067169189 2.70566177 5.63997269 0.067169189 -2.70566177 -5.63996696 0.067169189 -2.70566177;
	setAttr -s 48 ".ed[0:47]"  19 16 0 16 17 0 17 18 0 18 19 0 23 20 0 20 21 0
		 21 22 0 22 23 0 0 1 0 1 13 0 13 4 0 4 0 0 9 3 0 3 15 0 15 5 0 5 9 0 11 10 0 10 14 0
		 14 7 0 7 11 0 2 8 0 8 12 0 12 6 0 6 2 0 1 9 0 5 13 0 4 12 0 8 0 0 3 11 0 7 15 0 10 2 0
		 6 14 0 2 16 0 19 8 0 10 16 0 11 17 0 3 17 0 9 18 0 1 18 0 0 19 0 13 20 0 23 4 0 5 20 0
		 15 21 0 7 21 0 14 22 0 6 22 0 12 23 0;
	setAttr -s 26 -ch 96 ".fc[0:25]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 3
		f 4 4 5 6 7
		mu 0 4 4 5 6 7
		f 4 8 9 10 11
		mu 0 4 29 30 10 11
		f 4 12 13 14 15
		mu 0 4 31 26 14 15
		f 4 16 17 18 19
		mu 0 4 27 24 18 19
		f 4 20 21 22 23
		mu 0 4 25 28 22 23
		f 4 24 -16 25 -10
		mu 0 4 30 31 15 10
		f 4 26 -22 27 -12
		mu 0 4 11 22 28 29
		f 4 28 -20 29 -14
		mu 0 4 26 27 19 14
		f 4 30 -24 31 -18
		mu 0 4 24 25 23 18
		f 4 -21 32 -1 33
		mu 0 4 21 20 1 0
		f 3 -31 34 -33
		mu 0 3 20 17 1
		f 4 -17 35 -2 -35
		mu 0 4 17 16 2 1
		f 3 -29 36 -36
		mu 0 3 16 13 2
		f 4 -13 37 -3 -37
		mu 0 4 13 12 3 2
		f 3 -25 38 -38
		mu 0 3 12 9 3
		f 4 -9 39 -4 -39
		mu 0 4 9 8 0 3
		f 3 -28 -34 -40
		mu 0 3 8 21 0
		f 4 -11 40 -5 41
		mu 0 4 11 10 5 4
		f 3 -26 42 -41
		mu 0 3 10 15 5
		f 4 -15 43 -6 -43
		mu 0 4 15 14 6 5
		f 3 -30 44 -44
		mu 0 3 14 19 6
		f 4 -19 45 -7 -45
		mu 0 4 19 18 7 6
		f 3 -32 46 -46
		mu 0 3 18 23 7
		f 4 -23 47 -8 -47
		mu 0 4 23 22 4 7
		f 3 -27 -42 -48
		mu 0 3 22 11 4;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 24 
		0 0 
		1 0 
		2 0 
		3 0 
		4 0 
		5 0 
		6 0 
		7 0 
		8 0 
		9 0 
		10 0 
		11 0 
		12 0 
		13 0 
		14 0 
		15 0 
		16 0 
		17 0 
		18 0 
		19 0 
		20 0 
		21 0 
		22 0 
		23 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode materialInfo -n "materialInfo9";
	rename -uid "E8CF2BC4-4A62-0564-B222-3EA552F1C0F1";
createNode shadingEngine -n "lambert6SG";
	rename -uid "D2E99D5A-4555-90BE-9E07-5AA1176BC7F7";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode lambert -n "CardBack_Purple_mtl";
	rename -uid "C6B3C8F4-4523-B2E7-4888-668198703D0D";
createNode file -n "file4";
	rename -uid "175BA6F1-4CE0-3CB7-DD7E-ED9CC40E2096";
	setAttr ".ftn" -type "string" "D:/work/freelance/Bao/DoubleMoney/CardBack/Texture/CardBack_Purple_texture.tga";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture4";
	rename -uid "DC90B0EC-40C4-2155-A585-6EA5710615CE";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "46216DA1-4C9D-2338-D734-208378FD2CFC";
	setAttr -s 5 ".lnk";
	setAttr -s 5 ".slnk";
select -ne :time1;
	setAttr ".o" 0;
select -ne :hardwareRenderingGlobals;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 5 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 8 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 3 ".u";
select -ne :defaultRenderingList1;
select -ne :defaultTextureList1;
	setAttr -s 3 ".tx";
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultColorMgtGlobals;
	setAttr ".cfe" yes;
	setAttr ".cfp" -type "string" "<MAYA_RESOURCES>/OCIO-configs/Maya-legacy/config.ocio";
	setAttr ".vtn" -type "string" "sRGB gamma (legacy)";
	setAttr ".vn" -type "string" "sRGB gamma";
	setAttr ".dn" -type "string" "legacy";
	setAttr ".wsn" -type "string" "scene-linear Rec 709/sRGB";
	setAttr ".ovt" no;
	setAttr ".povt" no;
	setAttr ".otn" -type "string" "sRGB gamma (legacy)";
	setAttr ".potn" -type "string" "sRGB gamma (legacy)";
connectAttr "lambert6SG.msg" "materialInfo9.sg";
connectAttr "CardBack_Purple_mtl.msg" "materialInfo9.m";
connectAttr "file4.msg" "materialInfo9.t" -na;
connectAttr "CardBack_Purple_mtl.oc" "lambert6SG.ss";
connectAttr "CardBack_PurpleShape.iog" "lambert6SG.dsm" -na;
connectAttr "file4.oc" "CardBack_Purple_mtl.c";
connectAttr ":defaultColorMgtGlobals.cme" "file4.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "file4.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "file4.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "file4.ws";
connectAttr "place2dTexture4.c" "file4.c";
connectAttr "place2dTexture4.tf" "file4.tf";
connectAttr "place2dTexture4.rf" "file4.rf";
connectAttr "place2dTexture4.mu" "file4.mu";
connectAttr "place2dTexture4.mv" "file4.mv";
connectAttr "place2dTexture4.s" "file4.s";
connectAttr "place2dTexture4.wu" "file4.wu";
connectAttr "place2dTexture4.wv" "file4.wv";
connectAttr "place2dTexture4.re" "file4.re";
connectAttr "place2dTexture4.of" "file4.of";
connectAttr "place2dTexture4.r" "file4.ro";
connectAttr "place2dTexture4.n" "file4.n";
connectAttr "place2dTexture4.vt1" "file4.vt1";
connectAttr "place2dTexture4.vt2" "file4.vt2";
connectAttr "place2dTexture4.vt3" "file4.vt3";
connectAttr "place2dTexture4.vc1" "file4.vc1";
connectAttr "place2dTexture4.o" "file4.uv";
connectAttr "place2dTexture4.ofs" "file4.fs";
relationship "link" ":lightLinker1" "lambert6SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert6SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "lambert6SG.pa" ":renderPartition.st" -na;
connectAttr "CardBack_Purple_mtl.msg" ":defaultShaderList1.s" -na;
connectAttr "place2dTexture4.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "file4.msg" ":defaultTextureList1.tx" -na;
// End of Card_Purple.ma
